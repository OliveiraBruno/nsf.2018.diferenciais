﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var tela = new frmCorreio();
            tela.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var tela = new frmIbmVoz();
            tela.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var tela = new frmEmail();
            tela.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            var tela = new frmCriptografia();
            tela.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var tela = new frmSMS();
            tela.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            var tela = new frmTwitter();
            tela.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var tela = new frmImprimir();
            tela.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var tela = new frmCodBarras();
            tela.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var tela = new frmQrCode();
            tela.ShowDialog();
        }

        private void btnTempo_Click(object sender, EventArgs e)
        {
            var tela = new frmTempo();
            tela.ShowDialog();
        }
    }
}
