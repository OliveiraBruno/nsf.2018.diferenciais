﻿using nsf._2018.diferenciais.RedesSociais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmTwitter : Form
    {
        public frmTwitter()
        {
            InitializeComponent();
        }

        private void frmQrCode_Load(object sender, EventArgs e)
        {

        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            Twitter twitter = new Twitter();
            twitter.Enviar(txtMensagem.Text);

            MessageBox.Show("Tweet enviado com sucesso.", "Twitter", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnCarregar_Click(object sender, EventArgs e)
        {
            Twitter twitter = new Twitter();
            List<TweetResponse> tweets = twitter.Consultar();

            foreach (TweetResponse item in tweets)
            {
                lstTweets.Items.Add($"{item.Criacao.ToString("dd/MM HH:mm")}   {item.Usuario}");
                lstTweets.Items.Add($"{item.Mensagem}");
                lstTweets.Items.Add("");
            }
        }
    }
}
