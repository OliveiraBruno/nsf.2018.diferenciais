﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nsf._2018.diferenciais.RedesSociais
{
    class TweetResponse
    {
        public long Id { get; set; }
        public string Usuario { get; set; }
        public string Mensagem { get; set; }
        public DateTime Criacao { get; set; }
    }
}
