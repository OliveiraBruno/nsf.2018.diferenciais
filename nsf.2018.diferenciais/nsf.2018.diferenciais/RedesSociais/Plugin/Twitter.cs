﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;

namespace nsf._2018.diferenciais.RedesSociais
{
    class Twitter
    {
        // Esse plugin usa o NuGET Tweetinvi
        // Atribui as credenciais. Para conseguir as credenciais, cadastra-se em https://apps.twitter.com e https://developer.twitter.com
        // As credenciais abaixo são referentes a conta https://twitter.com/TecnicoEmInfor1

        string CONSUMER_KEY    = "Nclwufc9sOAIzm8IBLDG5IlMO";
        string CONSUMER_SECRET = "4S4XyPi5LUqpfohtfsjKaemoHPvDDU8VqsIVt4OETh0xHPVzM3";
        string ACCESS_TOKEN    = "1056325786339303424-gnmv3ShcaVd46LZthqkAaxYumqV5Xb";
        string ACCESS_SECRET   = "NWLOastM47CApO51qgOL0lBtk4HKqigb4Ra7URGuyMNqC";


        public Twitter()
        {
            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_SECRET);
        }


        public void Enviar(string mensagem)
        {
            // Publica Tweet 
            Tweet.PublishTweet(mensagem);
        }


        public List<TweetResponse> Consultar()
        {
            // Consulta Tweets da Timeline
            List<ITweet> tweets = Timeline.GetHomeTimeline().ToList();

            List<TweetResponse> tweetResponse = new List<TweetResponse>();
            foreach (ITweet item in tweets)
            {
                TweetResponse tweet = new TweetResponse();
                tweet.Id = item.Id;
                tweet.Usuario = item.CreatedBy.Name;
                tweet.Mensagem = item.FullText;
                tweet.Criacao = item.CreatedAt;

                tweetResponse.Add(tweet);
            }

            return tweetResponse;
        }
    }
}
