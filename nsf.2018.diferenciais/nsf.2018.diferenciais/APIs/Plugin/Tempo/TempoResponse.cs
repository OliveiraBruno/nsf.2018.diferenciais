﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nsf._2018.diferenciais.APIs
{
    class TempoResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public TempoData data { get; set; }
    }

    class TempoData
    {
        public decimal temperature { get; set; }
        public string wind_direction { get; set; }
        public decimal wind_velocity { get; set; }
        public decimal humidity { get; set; }
        public string condition { get; set; }
        public decimal pressure { get; set; }
        public string icon { get; set; }
        public decimal sensation { get; set; }
        public string date { get; set; }
    }

    class CidadeResponse
    {
        public string id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string country { get; set; }
    }

}
