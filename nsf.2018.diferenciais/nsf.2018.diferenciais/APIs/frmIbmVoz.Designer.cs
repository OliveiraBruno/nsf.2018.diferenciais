﻿namespace nsf._2018.diferenciais
{
    partial class frmIbmVoz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOuvir = new System.Windows.Forms.Button();
            this.txtOuvir = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblFalar = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnFalar = new System.Windows.Forms.Button();
            this.btnParar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOuvir
            // 
            this.btnOuvir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOuvir.Location = new System.Drawing.Point(20, 117);
            this.btnOuvir.Name = "btnOuvir";
            this.btnOuvir.Size = new System.Drawing.Size(288, 31);
            this.btnOuvir.TabIndex = 0;
            this.btnOuvir.Text = "Ouvir";
            this.btnOuvir.UseVisualStyleBackColor = true;
            this.btnOuvir.Click += new System.EventHandler(this.btnOuvir_Click);
            // 
            // txtOuvir
            // 
            this.txtOuvir.Location = new System.Drawing.Point(20, 27);
            this.txtOuvir.Multiline = true;
            this.txtOuvir.Name = "txtOuvir";
            this.txtOuvir.Size = new System.Drawing.Size(288, 81);
            this.txtOuvir.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnOuvir);
            this.groupBox1.Controls.Add(this.txtOuvir);
            this.groupBox1.Location = new System.Drawing.Point(24, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 164);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // lblFalar
            // 
            this.lblFalar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFalar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFalar.Location = new System.Drawing.Point(20, 65);
            this.lblFalar.Name = "lblFalar";
            this.lblFalar.Size = new System.Drawing.Size(288, 83);
            this.lblFalar.TabIndex = 14;
            this.lblFalar.Text = "[Valor]";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnParar);
            this.groupBox2.Controls.Add(this.btnFalar);
            this.groupBox2.Controls.Add(this.lblFalar);
            this.groupBox2.Location = new System.Drawing.Point(24, 178);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 164);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // btnFalar
            // 
            this.btnFalar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFalar.Location = new System.Drawing.Point(20, 27);
            this.btnFalar.Name = "btnFalar";
            this.btnFalar.Size = new System.Drawing.Size(142, 31);
            this.btnFalar.TabIndex = 0;
            this.btnFalar.Text = "Falar";
            this.btnFalar.UseVisualStyleBackColor = true;
            this.btnFalar.Click += new System.EventHandler(this.btnFalar_Click);
            // 
            // btnParar
            // 
            this.btnParar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParar.Location = new System.Drawing.Point(168, 27);
            this.btnParar.Name = "btnParar";
            this.btnParar.Size = new System.Drawing.Size(140, 31);
            this.btnParar.TabIndex = 15;
            this.btnParar.Text = "Parar";
            this.btnParar.UseVisualStyleBackColor = true;
            this.btnParar.Click += new System.EventHandler(this.btnParar_Click);
            // 
            // frmIbmVoz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 367);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmIbmVoz";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Diferenciais";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOuvir;
        private System.Windows.Forms.TextBox txtOuvir;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblFalar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnFalar;
        private System.Windows.Forms.Button btnParar;
    }
}

