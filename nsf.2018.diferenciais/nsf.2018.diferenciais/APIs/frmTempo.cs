﻿using nsf._2018.diferenciais.APIs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmTempo : Form
    {
        public frmTempo()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CorreioApi correioApi = new CorreioApi();
            CorreioResponse correio = correioApi.Buscar(txtCEP.Text);

            TempoApi tempoApi = new TempoApi();
            CidadeResponse cidade = tempoApi.BuscarCidade(correio.localidade);
            TempoResponse tempo   = tempoApi.BuscarTempo(cidade.id);


            lblCidade.Text = correio.localidade;
            lblTemperatura.Text = tempo.data.temperature.ToString();
            lblUmidade.Text = tempo.data.humidity.ToString();
            lblVeloc.Text = tempo.data.wind_velocity.ToString();
        }
    }
}
