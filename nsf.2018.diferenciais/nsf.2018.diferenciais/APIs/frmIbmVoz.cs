﻿using Newtonsoft.Json;
using nsf._2018.diferenciais.APIs.Plugin;
using Stannieman.AudioPlayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmIbmVoz : Form
    {
        public frmIbmVoz()
        {
            InitializeComponent();
        }

        IbmVoiceApi ibmApi = new IbmVoiceApi();

        private void btnOuvir_Click(object sender, EventArgs e)
        {
            ibmApi.Falar(txtOuvir.Text);
        }

        
        private void btnFalar_Click(object sender, EventArgs e)
        {
            ibmApi.IniciarOuvir();
        }

        private void btnParar_Click(object sender, EventArgs e)
        {
            string texto = ibmApi.PararOuvir();
            lblFalar.Text = texto;
        }
    }

   
}
