﻿namespace nsf._2018.diferenciais
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCorreio = new System.Windows.Forms.Button();
            this.btnIBm = new System.Windows.Forms.Button();
            this.btnTempo = new System.Windows.Forms.Button();
            this.btnSMS = new System.Windows.Forms.Button();
            this.btnEmail = new System.Windows.Forms.Button();
            this.btnQrCode = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnCodBarra = new System.Windows.Forms.Button();
            this.btnTwitter = new System.Windows.Forms.Button();
            this.btnCript = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCorreio
            // 
            this.btnCorreio.Location = new System.Drawing.Point(12, 12);
            this.btnCorreio.Name = "btnCorreio";
            this.btnCorreio.Size = new System.Drawing.Size(121, 30);
            this.btnCorreio.TabIndex = 0;
            this.btnCorreio.Text = "Correio";
            this.btnCorreio.UseVisualStyleBackColor = true;
            this.btnCorreio.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnIBm
            // 
            this.btnIBm.Location = new System.Drawing.Point(12, 48);
            this.btnIBm.Name = "btnIBm";
            this.btnIBm.Size = new System.Drawing.Size(121, 30);
            this.btnIBm.TabIndex = 1;
            this.btnIBm.Text = "IBM";
            this.btnIBm.UseVisualStyleBackColor = true;
            this.btnIBm.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnTempo
            // 
            this.btnTempo.Location = new System.Drawing.Point(12, 84);
            this.btnTempo.Name = "btnTempo";
            this.btnTempo.Size = new System.Drawing.Size(121, 30);
            this.btnTempo.TabIndex = 2;
            this.btnTempo.Text = "Tempo";
            this.btnTempo.UseVisualStyleBackColor = true;
            this.btnTempo.Click += new System.EventHandler(this.btnTempo_Click);
            // 
            // btnSMS
            // 
            this.btnSMS.Location = new System.Drawing.Point(132, 153);
            this.btnSMS.Name = "btnSMS";
            this.btnSMS.Size = new System.Drawing.Size(121, 30);
            this.btnSMS.TabIndex = 4;
            this.btnSMS.Text = "SMS";
            this.btnSMS.UseVisualStyleBackColor = true;
            this.btnSMS.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnEmail
            // 
            this.btnEmail.Location = new System.Drawing.Point(132, 117);
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(121, 30);
            this.btnEmail.TabIndex = 3;
            this.btnEmail.Text = "E-mail";
            this.btnEmail.UseVisualStyleBackColor = true;
            this.btnEmail.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnQrCode
            // 
            this.btnQrCode.Location = new System.Drawing.Point(12, 264);
            this.btnQrCode.Name = "btnQrCode";
            this.btnQrCode.Size = new System.Drawing.Size(121, 30);
            this.btnQrCode.TabIndex = 7;
            this.btnQrCode.Text = "QR Code";
            this.btnQrCode.UseVisualStyleBackColor = true;
            this.btnQrCode.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(12, 228);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(121, 30);
            this.btnImprimir.TabIndex = 6;
            this.btnImprimir.Text = "Impressão";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.button7_Click);
            // 
            // btnCodBarra
            // 
            this.btnCodBarra.Location = new System.Drawing.Point(12, 192);
            this.btnCodBarra.Name = "btnCodBarra";
            this.btnCodBarra.Size = new System.Drawing.Size(121, 30);
            this.btnCodBarra.TabIndex = 5;
            this.btnCodBarra.Text = "Cód. Barras";
            this.btnCodBarra.UseVisualStyleBackColor = true;
            this.btnCodBarra.Click += new System.EventHandler(this.button8_Click);
            // 
            // btnTwitter
            // 
            this.btnTwitter.Location = new System.Drawing.Point(132, 299);
            this.btnTwitter.Name = "btnTwitter";
            this.btnTwitter.Size = new System.Drawing.Size(121, 30);
            this.btnTwitter.TabIndex = 8;
            this.btnTwitter.Text = "Twitter";
            this.btnTwitter.UseVisualStyleBackColor = true;
            this.btnTwitter.Click += new System.EventHandler(this.button9_Click);
            // 
            // btnCript
            // 
            this.btnCript.Location = new System.Drawing.Point(12, 338);
            this.btnCript.Name = "btnCript";
            this.btnCript.Size = new System.Drawing.Size(121, 30);
            this.btnCript.TabIndex = 9;
            this.btnCript.Text = "Criptografia";
            this.btnCript.UseVisualStyleBackColor = true;
            this.btnCript.Click += new System.EventHandler(this.button10_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(265, 377);
            this.Controls.Add(this.btnCript);
            this.Controls.Add(this.btnTwitter);
            this.Controls.Add(this.btnQrCode);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnCodBarra);
            this.Controls.Add(this.btnSMS);
            this.Controls.Add(this.btnEmail);
            this.Controls.Add(this.btnTempo);
            this.Controls.Add(this.btnIBm);
            this.Controls.Add(this.btnCorreio);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Diferenciais";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCorreio;
        private System.Windows.Forms.Button btnIBm;
        private System.Windows.Forms.Button btnTempo;
        private System.Windows.Forms.Button btnSMS;
        private System.Windows.Forms.Button btnEmail;
        private System.Windows.Forms.Button btnQrCode;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnCodBarra;
        private System.Windows.Forms.Button btnTwitter;
        private System.Windows.Forms.Button btnCript;
    }
}

