﻿using Spire.Doc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmImprimir : Form
    {
        public frmImprimir()
        {
            InitializeComponent();
        }

        private void frmQrCode_Load(object sender, EventArgs e)
        {

        }

        private void btnCarregarDoc_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            DialogResult result = janela.ShowDialog();

            if (result == DialogResult.OK)
            {
                System.IO.FileInfo arquivo = new System.IO.FileInfo(janela.FileName);

                lblCaminho.Text = arquivo.FullName;
                lblNome.Text = arquivo.Name;
                lblTipo.Text = arquivo.Extension;
                lblData.Text = arquivo.CreationTime.ToString();
                lblTamanho.Text = (arquivo.Length / 1024).ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //ProcessStartInfo info = new ProcessStartInfo(lblCaminho.Text);
            //info.Verb = "Print";
            //info.CreateNoWindow = true;
            //info.WindowStyle = ProcessWindowStyle.Hidden;

            //Process.Start(info);
            Document doc = new Document(lblCaminho.Text, FileFormat.Auto);
            //doc.LoadFromFile();

            PrintDialog dialog = new PrintDialog();
            dialog.AllowPrintToFile = true;
            dialog.AllowCurrentPage = true;
            dialog.AllowSomePages = true;
            dialog.UseEXDialog = true;
            doc.PrintDialog = dialog;

            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                doc.PrintDocument.Print();
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
