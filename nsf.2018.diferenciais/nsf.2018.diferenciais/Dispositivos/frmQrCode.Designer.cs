﻿namespace nsf._2018.diferenciais
{
    partial class frmQrCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGerar = new System.Windows.Forms.Button();
            this.txtGerar = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnLer = new System.Windows.Forms.Button();
            this.txtLer = new System.Windows.Forms.TextBox();
            this.imgBarCode = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBarCode)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGerar
            // 
            this.btnGerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerar.Location = new System.Drawing.Point(20, 117);
            this.btnGerar.Name = "btnGerar";
            this.btnGerar.Size = new System.Drawing.Size(288, 31);
            this.btnGerar.TabIndex = 1;
            this.btnGerar.Text = "Gerar QRCode";
            this.btnGerar.UseVisualStyleBackColor = true;
            this.btnGerar.Click += new System.EventHandler(this.btnGerar_Click);
            // 
            // txtGerar
            // 
            this.txtGerar.Location = new System.Drawing.Point(20, 27);
            this.txtGerar.Multiline = true;
            this.txtGerar.Name = "txtGerar";
            this.txtGerar.Size = new System.Drawing.Size(288, 81);
            this.txtGerar.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGerar);
            this.groupBox1.Controls.Add(this.txtGerar);
            this.groupBox1.Location = new System.Drawing.Point(24, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 164);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnLer);
            this.groupBox2.Controls.Add(this.txtLer);
            this.groupBox2.Location = new System.Drawing.Point(24, 195);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 164);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // btnLer
            // 
            this.btnLer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLer.Location = new System.Drawing.Point(20, 116);
            this.btnLer.Name = "btnLer";
            this.btnLer.Size = new System.Drawing.Size(288, 31);
            this.btnLer.TabIndex = 3;
            this.btnLer.Text = "Ler QRCode";
            this.btnLer.UseVisualStyleBackColor = true;
            this.btnLer.Click += new System.EventHandler(this.btnLer_Click);
            // 
            // txtLer
            // 
            this.txtLer.Location = new System.Drawing.Point(20, 26);
            this.txtLer.Multiline = true;
            this.txtLer.Name = "txtLer";
            this.txtLer.Size = new System.Drawing.Size(288, 81);
            this.txtLer.TabIndex = 2;
            // 
            // imgBarCode
            // 
            this.imgBarCode.Location = new System.Drawing.Point(389, 128);
            this.imgBarCode.Name = "imgBarCode";
            this.imgBarCode.Size = new System.Drawing.Size(125, 117);
            this.imgBarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgBarCode.TabIndex = 6;
            this.imgBarCode.TabStop = false;
            // 
            // frmQrCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 370);
            this.Controls.Add(this.imgBarCode);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmQrCode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Diferenciais";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBarCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGerar;
        private System.Windows.Forms.TextBox txtGerar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnLer;
        private System.Windows.Forms.TextBox txtLer;
        private System.Windows.Forms.PictureBox imgBarCode;
    }
}

