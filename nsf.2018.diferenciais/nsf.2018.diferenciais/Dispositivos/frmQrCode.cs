﻿using nsf._2018.diferenciais.Dispositivos;
using Spire.Barcode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmQrCode : Form
    {
        public frmQrCode()
        {
            InitializeComponent();
        }


        private void btnGerar_Click(object sender, EventArgs e)
        {
            SaveFileDialog janela = new SaveFileDialog();
            janela.Filter = "Imagens (*.png)|*.png";

            DialogResult result = janela.ShowDialog();

            if (result == DialogResult.OK)
            {
                QRCodeService barcode = new QRCodeService();
                Image imagem = barcode.SalvarQrCode(txtGerar.Text, janela.FileName);

                imgBarCode.Image = imagem;
            }

        }

        private void btnLer_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            janela.Filter = "Imagens (*.png)|*.png";

            DialogResult result = janela.ShowDialog();

            if (result == DialogResult.OK)
            {
                QRCodeService barcode = new QRCodeService();
                string dado = barcode.LerQrCode(janela.FileName);

                txtLer.Text = dado;
            }

        }
    }
}
