﻿using nsf._2018.diferenciais.Dispositivos;
using Spire.Barcode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmCodBarras : Form
    {
        public frmCodBarras()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            SaveFileDialog janela = new SaveFileDialog();
            janela.Filter = "Imagens (*.png)|*.png";

            DialogResult result = janela.ShowDialog();

            if (result == DialogResult.OK)
            {
                BarCodeService barcode = new BarCodeService();
                Image imagem = barcode.SalvarCodigoBarras(txtGerar.Text, janela.FileName);

                imgBarCode.Image = imagem;
            }
        }

        private void btnLer_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            janela.Filter = "Imagens (*.png)|*.png";

            DialogResult result = janela.ShowDialog();

            if (result == DialogResult.OK)
            {
                BarCodeService barcode = new BarCodeService();
                string dado = barcode.LerCodigoBarras(janela.FileName);

                txtLer.Text = dado;
            }
        }
    }
}
