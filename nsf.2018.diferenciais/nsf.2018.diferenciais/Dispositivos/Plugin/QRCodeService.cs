﻿using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;
using Spire.Barcode;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;


namespace nsf._2018.diferenciais.Dispositivos
{
    class QRCodeService
    {
        // Esse plugin usa o NuGET MessagingToolkit.QRCode e Spire.Barcode


        public Image SalvarQrCode(string mensagem, string arquivo = null)
        {
            // Configura QRCode
            BarcodeSettings config = new BarcodeSettings();
            config.ShowText = false;
            config.ShowTopText = false;
            config.ShowCheckSumChar = false;
            config.ShowTextOnBottom = false;
            config.ShowStartCharAndStopChar = false;

            config.QRCodeDataMode = QRCodeDataMode.AlphaNumber;
            config.Type = BarCodeType.QRCode;
            config.QRCodeECL = QRCodeECL.H;
            config.X = 1.0f;

            // Atribui valor ao QRCode
            config.Data = mensagem;
            config.Data2D = mensagem;

            
            // Gera o QRCode
            BarCodeGenerator qrcode = new BarCodeGenerator(config);
            Image code = qrcode.GenerateImage();

            // Salva Imagem
            if (!string.IsNullOrEmpty(arquivo))
                code.Save(arquivo.Replace(".png", "") + ".png", ImageFormat.Png);

            // Retorna Imagem QRCode
            return code;
        }


        public string LerQrCode(string arquivo)
        {
            // Cria objeto para Ler QRCode
            QRCodeDecoder qrcode = new QRCodeDecoder();

            // Carrega Imagem do QRCode
            Bitmap imagem = Bitmap.FromStream(LerImagem(arquivo)) as Bitmap;

            // Lê o valor do QRCode
            string dado = qrcode.Decode(new QRCodeBitmapImage(imagem));
            return dado;
        }

        public string LerQrCode(Image imagem)
        {
            // Cria objeto para Ler QRCode
            QRCodeDecoder qrcode = new QRCodeDecoder();

            // Lê o valor do QRCode
            string dado = qrcode.Decode(new QRCodeBitmapImage(imagem as Bitmap));
            return dado;
        }


        private MemoryStream LerImagem(string arquivo)
        {
            MemoryStream ms = new MemoryStream();
            using (FileStream fs = new FileStream(arquivo, FileMode.Open, FileAccess.Read))
            {
                fs.CopyTo(ms);
            }
            return ms;
        }

    }
}
