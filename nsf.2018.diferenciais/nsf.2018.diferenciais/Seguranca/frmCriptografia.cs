﻿using nsf._2018.diferenciais.Seguranca;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmCriptografia : Form
    {
        public frmCriptografia()
        {
            InitializeComponent();
        }
        
        private void btnAESCript_Click(object sender, EventArgs e)
        {
            string chave = "chavecom_16_letr";

            AESCript aes = new AESCript();
            string cript = aes.Criptografar(chave, txtMensagem.Text);

            txtSimetrico.Text = cript;
        }

        private void btnAESDecript_Click(object sender, EventArgs e)
        {
            string chave = "chavecom_16_letr";

            AESCript aes = new AESCript();
            string cript = aes.Descriptografar(chave, txtMensagem.Text);

            txtSimetrico.Text = cript;
        }

        private void btnDESCript_Click(object sender, EventArgs e)
        {
            string chave = "com8letr";

            DESCript des = new DESCript();
            string cript = des.Criptografar(chave, txtMensagem.Text);

            txtSimetrico.Text = cript;
        }

        private void btnDESDecript_Click(object sender, EventArgs e)
        {
            string chave = "com8letr";

            DESCript des = new DESCript();
            string cript = des.Descriptografar(chave, txtMensagem.Text);

            txtSimetrico.Text = cript;
        }

        private void btnMD5Cript_Click(object sender, EventArgs e)
        {
            MD5Hash md5 = new MD5Hash();
            string cript = md5.Criptografar(txtMensagem.Text);

            txtHash.Text = cript;
        }

        private void btnSHA256_Click(object sender, EventArgs e)
        {
            SHA256Hash sha256 = new SHA256Hash();
            string cript = sha256.Criptografar(txtMensagem.Text);

            txtHash.Text = cript;
        }
    }
}
