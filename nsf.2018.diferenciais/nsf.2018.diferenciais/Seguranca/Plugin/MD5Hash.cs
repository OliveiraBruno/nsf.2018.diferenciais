﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace nsf._2018.diferenciais.Seguranca
{
    class MD5Hash
    {
        public string Criptografar(string mensagem)
        {
            // Cria objeto para criptografia MD5
            MD5 md5 = MD5.Create();

            // Transforma chave e mensagem em array de byts
            byte[] mensagemBytes = Encoding.UTF8.GetBytes(mensagem);

            // Realiza criptografia
            byte[] criptografiaBytes = md5.ComputeHash(mensagemBytes);

            // Tranforma criptografia em string
            string criptografia = Convert.ToBase64String(criptografiaBytes);

            return criptografia;
        }
    }
}
