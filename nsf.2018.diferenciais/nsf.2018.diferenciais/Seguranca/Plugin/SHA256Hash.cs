﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace nsf._2018.diferenciais.Seguranca
{
    class SHA256Hash
    {
        public string Criptografar(string mensagem)
        {
            // Cria objeto para criptografia SHA256
            SHA256 sha256 = SHA256.Create();
            
            // Transforma chave e mensagem em array de byts
            byte[] mensagemBytes = Encoding.UTF8.GetBytes(mensagem);
            
            // Realiza criptografia
            byte[] criptografiaBytes = sha256.ComputeHash(mensagemBytes);

            // Tranforma criptografia em string
            string criptografia = Convert.ToBase64String(criptografiaBytes);

            return criptografia;
        }
    }
}
