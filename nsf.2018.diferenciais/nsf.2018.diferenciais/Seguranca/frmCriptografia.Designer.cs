﻿namespace nsf._2018.diferenciais
{
    partial class frmCriptografia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDESDecript = new System.Windows.Forms.Button();
            this.btnDESCript = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAESDecript = new System.Windows.Forms.Button();
            this.txtSimetrico = new System.Windows.Forms.TextBox();
            this.btnAESCript = new System.Windows.Forms.Button();
            this.txtMensagem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSHA256 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHash = new System.Windows.Forms.TextBox();
            this.btnMD5Cript = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnDESDecript);
            this.groupBox1.Controls.Add(this.btnDESCript);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnAESDecript);
            this.groupBox1.Controls.Add(this.txtSimetrico);
            this.groupBox1.Controls.Add(this.btnAESCript);
            this.groupBox1.Location = new System.Drawing.Point(12, 140);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 205);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Simétrica";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(19, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 26);
            this.label2.TabIndex = 31;
            this.label2.Text = "DES";
            // 
            // btnDESDecript
            // 
            this.btnDESDecript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDESDecript.Location = new System.Drawing.Point(197, 73);
            this.btnDESDecript.Name = "btnDESDecript";
            this.btnDESDecript.Size = new System.Drawing.Size(136, 31);
            this.btnDESDecript.TabIndex = 30;
            this.btnDESDecript.Text = "Descriptografar";
            this.btnDESDecript.UseVisualStyleBackColor = true;
            this.btnDESDecript.Click += new System.EventHandler(this.btnDESDecript_Click);
            // 
            // btnDESCript
            // 
            this.btnDESCript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDESCript.Location = new System.Drawing.Point(79, 73);
            this.btnDESCript.Name = "btnDESCript";
            this.btnDESCript.Size = new System.Drawing.Size(112, 31);
            this.btnDESCript.TabIndex = 29;
            this.btnDESCript.Text = "Criptografar";
            this.btnDESCript.UseVisualStyleBackColor = true;
            this.btnDESCript.Click += new System.EventHandler(this.btnDESCript_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(19, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 26);
            this.label1.TabIndex = 28;
            this.label1.Text = "AES";
            // 
            // btnAESDecript
            // 
            this.btnAESDecript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAESDecript.Location = new System.Drawing.Point(197, 36);
            this.btnAESDecript.Name = "btnAESDecript";
            this.btnAESDecript.Size = new System.Drawing.Size(136, 31);
            this.btnAESDecript.TabIndex = 27;
            this.btnAESDecript.Text = "Descriptografar";
            this.btnAESDecript.UseVisualStyleBackColor = true;
            this.btnAESDecript.Click += new System.EventHandler(this.btnAESDecript_Click);
            // 
            // txtSimetrico
            // 
            this.txtSimetrico.BackColor = System.Drawing.SystemColors.Control;
            this.txtSimetrico.Location = new System.Drawing.Point(15, 119);
            this.txtSimetrico.Multiline = true;
            this.txtSimetrico.Name = "txtSimetrico";
            this.txtSimetrico.Size = new System.Drawing.Size(318, 72);
            this.txtSimetrico.TabIndex = 26;
            // 
            // btnAESCript
            // 
            this.btnAESCript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAESCript.Location = new System.Drawing.Point(79, 36);
            this.btnAESCript.Name = "btnAESCript";
            this.btnAESCript.Size = new System.Drawing.Size(112, 31);
            this.btnAESCript.TabIndex = 25;
            this.btnAESCript.Text = "Criptografar";
            this.btnAESCript.UseVisualStyleBackColor = true;
            this.btnAESCript.Click += new System.EventHandler(this.btnAESCript_Click);
            // 
            // txtMensagem
            // 
            this.txtMensagem.Location = new System.Drawing.Point(12, 39);
            this.txtMensagem.Multiline = true;
            this.txtMensagem.Name = "txtMensagem";
            this.txtMensagem.Size = new System.Drawing.Size(348, 80);
            this.txtMensagem.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F);
            this.label3.Location = new System.Drawing.Point(9, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 32;
            this.label3.Text = "Mensagem";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnSHA256);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtHash);
            this.groupBox2.Controls.Add(this.btnMD5Cript);
            this.groupBox2.Location = new System.Drawing.Point(12, 351);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(348, 205);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hash";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(19, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 26);
            this.label4.TabIndex = 31;
            this.label4.Text = "SHA";
            // 
            // btnSHA256
            // 
            this.btnSHA256.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSHA256.Location = new System.Drawing.Point(79, 73);
            this.btnSHA256.Name = "btnSHA256";
            this.btnSHA256.Size = new System.Drawing.Size(112, 31);
            this.btnSHA256.TabIndex = 29;
            this.btnSHA256.Text = "Criptografar";
            this.btnSHA256.UseVisualStyleBackColor = true;
            this.btnSHA256.Click += new System.EventHandler(this.btnSHA256_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(19, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 26);
            this.label5.TabIndex = 28;
            this.label5.Text = "MD5";
            // 
            // txtHash
            // 
            this.txtHash.BackColor = System.Drawing.SystemColors.Control;
            this.txtHash.Location = new System.Drawing.Point(15, 119);
            this.txtHash.Multiline = true;
            this.txtHash.Name = "txtHash";
            this.txtHash.Size = new System.Drawing.Size(318, 72);
            this.txtHash.TabIndex = 26;
            // 
            // btnMD5Cript
            // 
            this.btnMD5Cript.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMD5Cript.Location = new System.Drawing.Point(79, 36);
            this.btnMD5Cript.Name = "btnMD5Cript";
            this.btnMD5Cript.Size = new System.Drawing.Size(112, 31);
            this.btnMD5Cript.TabIndex = 25;
            this.btnMD5Cript.Text = "Criptografar";
            this.btnMD5Cript.UseVisualStyleBackColor = true;
            this.btnMD5Cript.Click += new System.EventHandler(this.btnMD5Cript_Click);
            // 
            // frmCriptografia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 571);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMensagem);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmCriptografia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Diferenciais";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMensagem;
        private System.Windows.Forms.Button btnAESCript;
        private System.Windows.Forms.TextBox txtSimetrico;
        private System.Windows.Forms.Button btnAESDecript;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDESDecript;
        private System.Windows.Forms.Button btnDESCript;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSHA256;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHash;
        private System.Windows.Forms.Button btnMD5Cript;
    }
}

