﻿using nsf._2018.diferenciais.Comunicacao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmSMS : Form
    {
        public frmSMS()
        {
            InitializeComponent();
        }
        
        private void btnEnviarSms_Click(object sender, EventArgs e)
        {
            TwilioSMS sms = new TwilioSMS();
            sms.Enviar(txtPara.Text, txtMensagem.Text);
        }
    }
}
