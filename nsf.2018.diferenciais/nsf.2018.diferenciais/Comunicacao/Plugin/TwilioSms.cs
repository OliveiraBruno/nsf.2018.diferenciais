﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace nsf._2018.diferenciais.Comunicacao
{
    class TwilioSMS
    {
        // Acesse https://www.twilio.com/, para criar uma conta e receber as credenciais

        string url           = "https://api.twilio.com/2010-04-01/Accounts/AC97ecc631f5e7464cf06627d84fc4dd41/Messages.json";
        string authorization = "AC97ecc631f5e7464cf06627d84fc4dd41:91f79011dd6428336bd7f47c2af4234e";
        string fromNumber    = "+16677714066";


        public async void Enviar(string celular, string mensagem)
        {
            // Prepara cliente para conectar na Api
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(authorization)));

            // Monta parâmetros da chamada Api
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("From", fromNumber);
            parms.Add("To", celular);
            parms.Add("Body", mensagem);

            FormUrlEncodedContent content = new FormUrlEncodedContent(parms);

            // Realiza chamada POST
            var response = await client.PostAsync(url, content);
        }
    }
}
