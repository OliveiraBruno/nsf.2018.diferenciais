﻿using nsf._2018.diferenciais.Comunicacao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nsf._2018.diferenciais
{
    public partial class frmEmail : Form
    {
        public frmEmail()
        {
            InitializeComponent();
        }


        Email email = new Email();


        private void btnEnviar_Click(object sender, EventArgs e)
        {
            email.Para = txtPara.Text;
            email.Assunto = txtAssunto.Text;
            email.Mensagem = txtMensagem.Text;

            email.Enviar();
        }

        private void lblAnexo_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();
            janela.ShowDialog();

            lstAnexos.Items.Add(janela.FileName);

            email.AdicionarAnexo(janela.FileName);
        }
    }
}
